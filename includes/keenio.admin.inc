<?php

/**
 * @file
 * Defines administrative forms and callbacks for the Keen IO module.
 */


/**
 * Builds the Keen IO settings form.
 */
function keenio_settings_form($form, &$form_state) {
  $form['api'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default API credentials'),
    '#collapsible' => FALSE,
  );

  $form['api']['keenio_default_project_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Project ID'),
    '#default_value' => variable_get('keenio_default_project_id', ''),
  );

  $form['api']['keenio_default_write_key'] = array(
    '#type' => 'textarea',
    '#title' => t('Write key'),
    '#default_value' => variable_get('keenio_default_write_key', ''),
    '#rows' => 2,
  );

  $form['api']['keenio_default_read_key'] = array(
    '#type' => 'textarea',
    '#title' => t('Read key'),
    '#default_value' => variable_get('keenio_default_read_key', ''),
    '#rows' => 2,
  );

  return system_settings_form($form);
}
